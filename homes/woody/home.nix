{
  inputs,
  outputs,
  pkgs,
  ...
}:
let
  catflavor = "mocha";
  cataccent = "maroon";
  shackra-mu-index = pkgs.writeShellScriptBin "shackra-mu-index" ''
    if pgrep -f 'mu server'; then
      emacsclient -e '(mu4e-update-index)'
    else
      mu index
    fi
  '';
in
{
  nixpkgs = {
    overlays = [ outputs.overlays.unstable-packages ];
    config.allowUnfree = true;
  };

  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "jorge";
  home.homeDirectory = "/home/jorge";

  imports = [
    ../../modules
    inputs.sops-nix.homeManagerModules.sops
  ];

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.11"; # Please read the comment before changing.

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages =
    with pkgs;
    [
      transmission-remote-gtk
      helix
      butane
      shackra-mu-index
    ]
    ++ [
      inputs.fh.packages.${system}.default
      inputs.nix-search.packages.${system}.default
    ];

  # activa el tema catppuccin en todos los programas posibles, los cuales son:
  # - zellij
  # - bat
  # - rofi
  # - hyprland
  # - starship
  # - waybar
  catppuccin = {
    enable = true;
    flavor = catflavor;
    accent = cataccent;
    cursors = {
      enable = true;
      accent = cataccent;
      flavor = catflavor;
    };
    gtk = {
      enable = true;
      accent = cataccent;
      flavor = catflavor;
      icon = {
        enable = true;
        accent = cataccent;
        flavor = catflavor;
      };
    };
  };

  home = {
    # ajusta $PATH
    sessionPath = [
      "$HOME/go/bin" # Golang
      "$HOME/.cargo/bin" # Rust
      "$HOME/.local/bin" # varios, etc.
    ];
    sessionVariables = {
      SOPS_PGP_FP = "1E12 2246 6B30 776E 0A48 1377 CD3E 0427 6FC1 C1A1";
      STEAM_EXTRA_COMPAT_TOOLS_PATHS = "$HOME/.steam/root/compatibilitytools.d";
      XDG_SCREENSHOTS_DIR = "$HOME/Imágenes/Capturas de pantalla/";
      WLR_NO_HARDWARE_CURSORS = 1; # https://www.reddit.com/r/NixOS/comments/105f4e0/invisible_cursor_on_hyprland/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button
    };
    pointerCursor = {
      gtk.enable = true;
      x11.enable = true;
      size = 32;
    };
  };

  programs.zsh = {
    dirHashes = {
      dev = "$HOME/code";
      work = "$HOME/code/work";
      gamedev = "$HOME/code/servants of the secret fire";
      crypto = "$HOME/code/crypto";
    };
    initExtra = ''
      get_pid() {
        if [[ -z "$1" ]]; then
          echo "Usage: get_pid <pattern>"
          return 1
        fi
        ps aux | grep "$1" | grep -v grep | awk '{print $2}' | head -n 1
      }
    '';
  };

  programs.git = {
    signing = {
      key = "CD3E04276FC1C1A1";
      signByDefault = false;
    };

    ignores = [
      "*~"
      "*.swp"
      ".ccls-cache"
      "compile_commands.json"
      ".tool-versions"
      ".DS_Store"
      ".venv"
      ".direnv"
    ];

    extraConfig = {
      safe.directory = [ "/etc/nixos" ];
    };
  };

  services.gpg-agent = {
    enable = true;

    defaultCacheTtl = 86400;
    defaultCacheTtlSsh = 86400;
    maxCacheTtl = 86400;
    maxCacheTtlSsh = 86400;
    enableSshSupport = true;
  };

  services.syncthing = {
    enable = true;
  };
  services.imapnotify.enable = true;

  programs.mbsync.enable = true;

  programs.mu = {
    enable = true;
    package = pkgs.unstable.mu;
  };

  accounts.email.accounts = {
    # Cuenta personal en ProtonMail
    principal = {
      primary = true;
      address = "jorge@esavara.cr";
      realName = "Jorge Araya";
      userName = "jorge@esavara.cr";
      passwordCommand = "echo p2nKFAgpSZ9QYJlYn6_UBg";
      imap = {
        host = "127.0.0.1";
        port = 1143;
        tls = {
          enable = true;
          useStartTls = true;
          #certificatesFile = ~/.config/protonmail/bridge-v3/cert.pem;
        };
      };
      smtp = {
        host = "127.0.0.1";
        port = 1025;
        tls = {
          enable = true;
          useStartTls = true;
          #certificatesFile = ~/.config/protonmail/bridge-v3/cert.pem;
        };
      };
      mbsync = {
        enable = true;
        create = "both";
        expunge = "both";
        remove = "both";
        extraConfig = {
          account = {
            CertificateFile = "~/.config/protonmail/bridge-v3/cert.pem";
          };
          channel = {
            Patterns = [
              "INBOX"
              "Archive"
              "Trash"
              "Spam"
              "Drafts"
              "Sent"
              "Labels/lista de correo"
              "Folders/Transacciones y transferencias"
            ];
            CopyArrivalDate = "yes";
            Create = "Both";
            Expunge = "Both";
            SyncState = "*";
          };
        };
      };
      mu = {
        enable = true;
      };
      imapnotify = {
        enable = true;
        boxes = [ "inbox" ];
        onNotify = "${pkgs.isync}/bin/mbsync principal";
        onNotifyPost = "shackra-mu-index";
        extraConfig = {
          tls = true;
          tlsOption = {
            starttls = true;
          };
        };
      };
      msmtp = {
        enable = true;
        extraConfig = {
          tls_trust_file = "~/.config/protonmail/bridge-v3/cert.pem";
          tls_starttls = "on";
        };
      };
    };

    # Configuración de cuenta de Gmail
    gmail = {
      flavor = "gmail.com";
      address = "shackrasislock0@gmail.com";
      realName = "Jorge Araya";
      passwordCommand = "cat /run/secrets/email/gmail/password";
      mu.enable = true;
      imap = {
        host = "imap.gmail.com";
        port = 993;
        tls.enable = true;
      };
      smtp = {
        host = "smtp.gmail.com";
        tls.enable = true;
      };
      mbsync = {
        enable = true;
        create = "both";
        expunge = "both";
        remove = "both";
        groups = {
          # Definimos el grupo Gmail
          Gmail = {
            channels = {
              inbox = {
                farPattern = "INBOX"; # Correo remoto (INBOX)
                nearPattern = "INBOX"; # Correo local (INBOX)
                extraConfig = {
                  CopyArrivalDate = "yes";
                  Create = "Both";
                  Expunge = "Both";
                  SyncState = "*";
                };
              };
              sent = {
                farPattern = "[Gmail]/Enviados";
                nearPattern = "Sent";
                extraConfig = {
                  CopyArrivalDate = "yes";
                  Create = "Both";
                  Expunge = "Both";
                  SyncState = "*";
                };
              };
              trash = {
                farPattern = "[Gmail]/Papelera"; # Correo remoto (Papelera)
                nearPattern = "Trash"; # Correo local (Papelera)
                extraConfig = {
                  CopyArrivalDate = "yes";
                  Create = "Both";
                  Expunge = "Both";
                  SyncState = "*";
                };
              };
              spam = {
                farPattern = "[Gmail]/Spam"; # Correo remoto (Spam)
                nearPattern = "Spam"; # Correo local (Spam)
                extraConfig = {
                  CopyArrivalDate = "yes";
                  Create = "Both";
                  Expunge = "Both";
                  SyncState = "*";
                };
              };
            };
          };
        };
      };
      imapnotify = {
        enable = true;
        boxes = [ "Inbox" ];
        onNotify = "${pkgs.isync}/bin/mbsync Gmail";
        onNotifyPost = "shackra-mu-index";
      };
      msmtp = {
        enable = true;
      };
    };

    # Configuración de cuenta Yahoo
    yahoo = {
      address = "jorgejavieran@yahoo.com.mx";
      userName = "jorgejavieran@yahoo.com.mx";
      realName = "Jorge Araya";
      passwordCommand = "cat /run/secrets/email/yahoo/password";
      mu.enable = true;
      imap = {
        host = "imap.mail.yahoo.com";
        port = 993;
        tls.enable = true;
      };
      smtp = {
        host = "smtp.mail.yahoo.com";
        port = 587;
        tls.enable = true;
      };
      mbsync = {
        enable = true;
        create = "both";
        expunge = "both";
        remove = "both";
        groups = {
          yahoo = {
            channels = {
              spam = {
                farPattern = "Bulk";
                nearPattern = "Spam";
                extraConfig = {
                  CopyArrivalDate = "yes";
                  Create = "Both";
                  Expunge = "Both";
                  SyncState = "*";
                };
              };
              drafts = {
                farPattern = "Draft";
                nearPattern = "Drafts";
                extraConfig = {
                  CopyArrivalDate = "yes";
                  Create = "Both";
                  Expunge = "Both";
                  SyncState = "*";
                };
              };
            };
          };
        };
        extraConfig = {
          channel = {
            Patterns = [
              "INBOX"
              "Archive/*"
              "Trash"
              "Sent"
            ];
            CopyArrivalDate = "yes";
            Create = "Both";
            Expunge = "Both";
            SyncState = "*";
          };
        };
      };
      imapnotify = {
        enable = true;
        boxes = [ "Inbox" ];
        onNotify = "${pkgs.isync}/bin/mbsync yahoo";
        onNotifyPost = "shackra-mu-index";
      };

      msmtp = {
        enable = true;
      };
    };

    # trabajo
    work = {
      flavor = "gmail.com";
      address = "jorge.araya@conductorone.com";
      realName = "Jorge Araya";
      passwordCommand = "cat /run/secrets/email/work/password";
      mu.enable = true;
      imap = {
        host = "imap.gmail.com";
        port = 993;
        tls.enable = true;
      };
      smtp = {
        host = "smtp.gmail.com";
        tls.enable = true;
      };
      mbsync = {
        enable = true;
        create = "both";
        expunge = "both";
        remove = "both";
        groups = {
          # Definimos el grupo Gmail para el trabajo
          work = {
            channels = {
              inbox = {
                farPattern = "INBOX"; # Correo remoto (INBOX)
                nearPattern = "INBOX"; # Correo local (INBOX)
                extraConfig = {
                  CopyArrivalDate = "yes";
                  Create = "Both";
                  Expunge = "Both";
                  SyncState = "*";
                };
              };
              sent = {
                farPattern = "[Gmail]/Enviados";
                nearPattern = "Sent";
                extraConfig = {
                  CopyArrivalDate = "yes";
                  Create = "Both";
                  Expunge = "Both";
                  SyncState = "*";
                };
              };
              trash = {
                farPattern = "[Gmail]/Papelera"; # Correo remoto (Papelera)
                nearPattern = "Trash"; # Correo local (Papelera)
                extraConfig = {
                  CopyArrivalDate = "yes";
                  Create = "Both";
                  Expunge = "Both";
                  SyncState = "*";
                };
              };
              spam = {
                farPattern = "[Gmail]/Spam"; # Correo remoto (Spam)
                nearPattern = "Spam"; # Correo local (Spam)
                extraConfig = {
                  CopyArrivalDate = "yes";
                  Create = "Both";
                  Expunge = "Both";
                  SyncState = "*";
                };
              };
            };
          };
        };
      };
      imapnotify = {
        enable = true;
        boxes = [ "Inbox" ];
        onNotify = "${pkgs.isync}/bin/mbsync work";
        onNotifyPost = "shackra-mu-index";
      };
      msmtp = {
        enable = true;
      };
    };
  };

  programs.msmtp.enable = true;

  services.flameshot = {
    enable = false; # no funciona en hyprland, ver https://github.com/flameshot-org/flameshot/issues/2978
    settings = {
      General = {
        disabledTrayIcon = false;
        showStartupLaunchMessage = false;
      };
    };
  };

  programs.hyprlock = {
    enable = true;
    settings = {
      general = {
        disable_loading_bar = true;
        hide_cursor = false;
        no_fade_in = false;
      };

      background = [
        {
          path = "screenshot";
          blur_passes = 3;
          blur_size = 12;
        }
      ];

      input-field = [
        {
          size = "400, 50";
          position = "0, -80";
          monitor = "DP-1";
          dots_center = true;
          fade_on_empty = false;
          font_color = "rgb(202, 211, 245)";
          inner_color = "rgb(91, 96, 120)";
          outer_color = "rgb(24, 25, 38)";
          outline_thickness = 2;
          placeholder_text = ''Clave...'';
          shadow_passes = 1;
        }
      ];
    };
  };

  services.hypridle = {
    enable = true;
    settings = {
      general = {
        after_sleep_cmd = "hyprctl dispatch dpms on";
        ignore_dbus_inhibit = false;
        lock_cmd = "hyprlock";
      };

      listener = [
        {
          timeout = 900;
          on-timeout = "hyprlock";
        }
        {
          timeout = 1200;
          on-timeout = "hyprctl dispatch dpms off";
          on-resume = "hyprctl dispatch dpms on";
        }
      ];
    };
  };

  wayland.windowManager.hyprland = {
    enable = true;
    package = pkgs.hyprland;
    plugins = [
      pkgs.hyprlandPlugins.hyprfocus
      pkgs.hyprlandPlugins.hyprscroller
    ];
    extraConfig = ''
      source = ~/.config/hypr/monitors.conf
      source = ~/.config/hypr/workspaces.conf

      animations {
          enabled = true
          bezier = myBezier, 0.05, 0.9, 0.1, 1.05
          animation = windows, 1, 3, myBezier
          animation = windowsOut, 1, 1, default, popin 80%
          animation = border, 1, 10, default
          animation = borderangle, 1, 8, default
          animation = fade, 1, 2, default
          animation = workspaces, 1, 5, default
      }

      plugins {
          hyprfocus {
              enabled = yes
              animate_floating = yes
              animate_workspacechange = yes
              focus_animation = shrink

              # Beziers for focus animations
              bezier = bezIn, 0.5,0.0,1.0,0.5
              bezier = bezOut, 0.0,0.5,0.5,1.0
              bezier = overshot, 0.05, 0.9, 0.1, 1.05
              bezier = smoothOut, 0.36, 0, 0.66, -0.56
              bezier = smoothIn, 0.25, 1, 0.5, 1
              bezier = realsmooth, 0.28,0.29,.69,1.08

              # Flash settings
              flash {
                  flash_opacity = 0.95
                  in_bezier = realsmooth
                  in_speed = 0.5
                  out_bezier = realsmooth
                  out_speed = 3
              }

              # Shrink settings
              shrink {
                  shrink_percentage = 0.95
                  in_bezier = realsmooth
                  in_speed = 1
                  out_bezier = realsmooth
                  out_speed = 2
              }
          }
      }
    '';
    settings = {
      "$mod" = "SUPER";
      cursor = {
        no_warps = true;
      };
      input = {
        kb_layout = "us,es";
        kb_options = "grp:win_space_toggle";
        follow_mouse = 0;
        focus_on_close = 1;
        mouse_refocus = false;
      };
      general = {
        layout = "scroller"; # hyprscroller debe estar instalado
        border_size = 0;
        gaps_in = 10;
        gaps_out = 22;
      };
      decoration = {
        rounding = 5;
        dim_inactive = true;
        dim_strength = 0.3;
        shadow = {
          enabled = true;
        };
        blur = {
          special = true;
        };
      };
      exec-once = [
        "systemctl --user start hyprpolkitagent"
        "walker --gapplication-service"
        ''dex --autostart --search-paths "$HOME/.config/autostart"''
        "hyprwall -r"
        "swaync"
        "waybar"
        "alacritty --daemon -e ${pkgs.writeShellScript "scroller_mode_listener" ''
          MODE_FILE="/tmp/hyprland_scroller_mode"

          function handle {
            if [[ ''${1:0:8} == "scroller" ]]; then
              if [[ ''${1:10:9} == "mode, row" ]]; then
                  echo "Row" > "$MODE_FILE"
              elif [[ ''${1:10:12} == "mode, column" ]]; then
                  echo "Column" > "$MODE_FILE"
              #else
                  #echo "" > "$MODE_FILE"
              fi
              hyprctl dispatch submap reset && pkill -SIGRTMIN+8 waybar # update the widget on waybar
            fi
          }

          # Initialize the mode file
          echo "" > "$MODE_FILE"

          socat - "UNIX-CONNECT:$XDG_RUNTIME_DIR/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock" | while read -r line; do handle "$line"; done''}"
      ];
      bindm = [
        # botones del raton
        "$mod, mouse:272, movewindow"
        "$mod, mouse:273, resizewindow"
      ];
      bind =
        [
          # ** aplicaciones **
          "$mod_CTRL,m,exec,emacsclient -c"
          "$mod_CTRL,b,exec,firefox"
          "$mod_ALT,b,exec,brave"
          "$mod,d,exec,walker"
          "$mod_CTRL,Return,exec, alacritty"
          ",print,exec,grimblast copysave area"
          "ALT,print,exec,grimblast copysave output"
          # ** Hyprland **
          "$mod,Backspace, killactive"
          "$mod,l,exec,hyprlock"
          "$mod,f,fullscreen"
          "$mod,home,togglespecialworkspace"
          "$mod_SHIFT,home,movetoworkspace,special"
          # cambiar foco
          "$mod, left, movefocus, l"
          "$mod, right, movefocus, r"
          "$mod, up, movefocus, u"
          "$mod, down, movefocus, d"
          # mover ventana
          "$mod_SHIFT, left,  movewindow, l"
          "$mod_SHIFT, right, movewindow, r"
          "$mod_SHIFT, up,    movewindow, u"
          "$mod_SHIFT, down,  movewindow, d"
          # redimensionar ventana activa
          "$mod_CTRL, left,  resizeactive, -20 0"
          "$mod_CTRL, right, resizeactive, 20 0"
          "$mod_CTRL, up,    resizeactive, 0 -20"
          "$mod_CTRL, down,  resizeactive, 0 20"
          # ciclar foco entre ventanas
          "$mod,Tab,cyclenext,"
          "$mod,Tab,bringactivetotop,"
          # hyprscroller
          "$mod_ALT,e,scroller:expelwindow"
          "$mod_ALT,a,scroller:admitwindow"
          "$mod_ALT,o,scroller:toggleoverview"
          "$mod_ALT,c,scroller:setmode, col"
          "$mod_ALT,r,scroller:setmode, row"
          "$mod_ALT,s,scroller:cyclesize, prev"
          "$mod_ALT,d,scroller:cyclesize, next"
          "$mod_ALT,f,scroller:fitsize, active"
          "$mod_ALT,pause,scroller:alignwindow,center"
          # ciclar entre maquetaciones
          "$mod_ALT,m,exec,${pkgs.writeShellScript "switch-hyprland-layout" ''
            case "$(hyprctl -j getoption general:layout | jq -r .str)" in
                dwindle) hyprctl keyword general:layout master  ;;
                master)  hyprctl keyword general:layout scroller ;;
                scroller) hyprctl keyword general:layout dwindle ;;
            esac
          ''}"
        ]
        ++ (builtins.concatLists (
          builtins.genList (
            i:
            let
              ws = i + 1;
            in
            [
              "$mod, code:1${toString i}, workspace, ${toString ws}"
              "$mod SHIFT, code:1${toString i}, movetoworkspace, ${toString ws}"
            ]
          ) 9
        ));

      windowrulev2 = [
        "fullscreen,monitor DP-1, initialClass:^(steam_app_.*)$"
        "center,initialTitle:^(Iniciando...)$"
        "workspace special silent,initialClass:^(signal)$"
        "workspace special silent,initialClass:^(discord)$"
        "workspace special silent,initialClass:^(org\.telegram\.desktop)$"
      ];
    };
  };

  programs.waybar = {
    enable = true;
    settings = [
      {
        layer = "top";
        position = "top";
        height = 30;
        output = [
          "DP-1"
        ];
        modules-center = [
          "custom/weather"
          "clock"
          "hyprland/language"
        ];
        modules-left = [
          "custom/hyprland-layout"
          "custom/scroller-mode"
          "hyprland/workspaces"
          "hyprland/window"
        ];
        modules-right = [
          "cpu"
          "memory"
          "temperature"
          "pulseaudio"
          "tray"
          "custom/notification"
        ];
        "clock" = {
          "format" = "   {:%H:%M     %e %b}";
          "tooltip-format" = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
          "today-format" = "<b>{}</b>";
          "on-click" = "gnome-calendar";
        };
        "hyprland/window" = {
          max-length = 22;
          separate-outputs = true;
          rewrite = {
            "" = " 🙈 No Windows? ";
          };
        };
        "tray" = {
          spacing = 12;
        };
        "cpu" = {
          interval = "1";
          format = "  {max_frequency}GHz <span color=\"darkgray\">| {usage}%</span>";
          max-length = 13;
          min-length = 13;
          on-click = "kitty -e htop --sort-key PERCENT_CPU";
          tooltip = false;
        };
        "temperature" = {
          # thermal-zone = 1;  # Comentado en el original, lo mantenemos así
          interval = "4";
          hwmon-path = "/sys/devices/pci0000:00/0000:00:18.3/hwmon/hwmon2/temp1_input";
          critical-threshold = 74;
          format-critical = "  {temperatureC}°C";
          format = "{icon}  {temperatureC}°C";
          format-icons = [
            ""
            ""
            ""
          ];
          max-length = 7;
          min-length = 7;
        };
        "bluetooth" = {
          interval = 30;
          format = "{icon}";
          # format-alt = "{status}";  # Comentado en el original
          format-icons = {
            enabled = "";
            disabled = "";
          };
          on-click = "blueman";
        };
        "network" = {
          # interface = "wlan0";  # Comentado en el original
          format-wifi = "  {essid}";
          format-ethernet = "{ifname}: {ipaddr}/{cidr} ";
          format-linked = "{ifname} (No IP) ";
          format-disconnected = "";
          format-alt = "{ifname}: {ipaddr}/{cidr}";
          family = "ipv4";
          tooltip-format-wifi = ''
              {ifname} @ {essid}
            IP: {ipaddr}
            Strength: {signalStrength}%
            Freq: {frequency}MHz
             {bandwidthUpBits}  {bandwidthDownBits}
          '';
          tooltip-format-ethernet = ''
             {ifname}
            IP: {ipaddr}
             {bandwidthUpBits}  {bandwidthDownBits}
          '';
        };
        "pulseaudio" = {
          scroll-step = 3; # %, puede ser un float
          format = "{icon} {volume}% {format_source}";
          format-bluetooth = "{volume}% {icon} {format_source}";
          format-bluetooth-muted = " {icon} {format_source}";
          format-muted = " {format_source}";
          # format-source = "{volume}% ";  # Comentado en el original
          # format-source-muted = "";  # Comentado en el original
          format-source = "";
          format-source-muted = "";
          format-icons = {
            headphone = "";
            hands-free = "";
            headset = "";
            phone = "";
            portable = "";
            car = "";
            default = [
              ""
              ""
              ""
            ];
          };
          on-click = "pavucontrol";
          on-click-right = "pactl set-source-mute @DEFAULT_SOURCE@ toggle";
        };
        "custom/weather" = {
          exec = "curl 'https://wttr.in/Siquirres?format=1'";
          interval = 3600;
        };
        "custom/notification" = {
          tooltip = false;
          format = "{} {icon}";
          format-icons = {
            notification = "<span foreground='red'><sup></sup></span>";
            none = "";
            dnd-notification = "<span foreground='red'><sup></sup></span>";
            dnd-none = "";
            inhibited-notification = "<span foreground='red'><sup></sup></span>";
            inhibited-none = "";
            dnd-inhibited-notification = "<span foreground='red'><sup></sup></span>";
            dnd-inhibited-none = "";
          };
          return-type = "json";
          exec-if = "which swaync-client";
          exec = "swaync-client -swb";
          on-click = "swaync-client -t -sw";
          on-click-right = "swaync-client -d -sw";
          escape = true;
        };
        "custom/hyprland-layout" = {
          exec = "hyprctl -j getoption general:layout | jq -r .str";
          interval = 1;
        };
        "custom/scroller-mode" = {
          return-type = "json";
          interval = "once";
          signal = 8;
          format = "{icon}";
          format-icons = [
            ""
            ""
          ];
          exec = pkgs.writeShellScript "scroller_mode_reader" ''
            MODE_FILE="/tmp/hyprland_scroller_mode"

            current_mode=$(cat "$MODE_FILE")

            if [[ "$current_mode" == "Row" ]]; then
                icon="Row"
                percent=0
                class="mode-row"
            elif [[ "$current_mode" == "Column" ]]; then
                icon="Column"
                percent=100
                class="mode-column"
            else
                icon="Row"
                percent=0
                class=""
            fi

            echo "{\"text\":\"$icon\", \"tooltip\":\"Scroller Mode: $current_mode\", \"class\":\"$class\",\"percentage\": $percent}"
          '';
        };
      }
    ];
    style = pkgs.lib.mkForce ./waybar_style.css;
  };

  programs.wofi = {
    enable = true;
    style = ''
      * {
      	font-family: "Iosevka Comfy", monospace;
      }

      window {
      	background-color: #3B4252;
      }

      #input {
      	margin: 5px;
      	border-radius: 0px;
      	border: none;
      	background-color: #3B4252;
      	color: white;
      }

      #inner-box {
      	background-color: #383C4A;
      }

      #outer-box {
      	margin: 2px;
      	padding: 10px;
      	background-color: #383C4A;
      }

      #scroll {
      	margin: 5px;
      }

      #text {
      	padding: 4px;
      	color: white;
      }

      #entry:nth-child(even){
      	background-color: #404552;
      }

      #entry:selected {
      	background-color: #4C566A;
      }

      #text:selected {
      	background: transparent;
      }
    '';
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
