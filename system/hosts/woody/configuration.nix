# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{
  outputs,
  config,
  pkgs,
  inputs,
  ...
}:
let
  # monitorsXmlContent = builtins.readFile /home/jorge/.config/monitors.xml;
  # monitorsConfig = pkgs.writeText "gdm_monitors.xml" monitorsXmlContent;

  raid0 = ''
    DEVICE partitions
    ARRAY /dev/md0 metadata=1.2 name=woody:0 UUID=b3210ef7:588e10d0:b7ada7d6:a167b92f
  '';
in
{
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    inputs.ucodenix.nixosModules.default
  ];

  services.ucodenix = {
    enable = true;
    cpuModelId = "00870F10";
  };

  nixpkgs = {
    overlays = [ outputs.overlays.unstable-packages ];
    config = {
      allowUnfree = true;
      permittedInsecurePackages = [
        "aspnetcore-runtime-6.0.36"
        "aspnetcore-runtime-wrapped-6.0.36"
        "dotnet-sdk-6.0.428"
        "dotnet-sdk-wrapped-6.0.428"
      ];
    };
  };

  nix = {
    settings = {
      experimental-features = [
        "nix-command"
        "flakes"
      ];
      substituters = [ "https://nix-gaming.cachix.org" ];
      trusted-public-keys = [ "nix-gaming.cachix.org-1:nbjlureqMbRAxR1gJ/f3hxemL9svXaZF/Ees8vCUUs4=" ];
    };
    optimise.automatic = true;
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 8d";
    };
  };

  sops.defaultSopsFile = ../../secrets/claves.yaml;
  sops.defaultSopsFormat = "yaml";
  sops.age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];

  sops.secrets = {
    "users/jorge/password_file" = {
      neededForUsers = true;
    };
    "email/gmail/username" = {
      owner = config.users.users.jorge.name;
    };
    "email/gmail/password" = {
      owner = config.users.users.jorge.name;
    };
    "email/yahoo/username" = {
      owner = config.users.users.jorge.name;
    };
    "email/yahoo/password" = {
      owner = config.users.users.jorge.name;
    };
    "email/work/password" = {
      owner = config.users.users.jorge.name;
    };
  };
  # Bootloader.
  boot = {
    swraid = {
      enable = true;
      mdadmConf = raid0;
    };
    tmp = {
      cleanOnBoot = true;
    };
    loader = {
      systemd-boot.enable = true;
      timeout = 30;
    };
    supportedFilesystems = [ "ntfs" ];
    # Copied from virtualisation.lxd.recommendedSysctlSettings
    initrd = {
      kernelModules = [
        "vfio_pci"
        "vfio"
        "vfio_iommu_type1"
        "fuse"
        "amdgpu"
      ];
      luks.devices."luks-2b5061e0-ef89-4faa-93ae-7dc90f45e716".device =
        "/dev/disk/by-uuid/2b5061e0-ef89-4faa-93ae-7dc90f45e716";
    };
    kernelParams = [
      "amd_iommu=on"
      "iommu=pt"
      "kvm_amd.npt=1"
      "kvm_amd.avic=1"
      "quiet"
      "splash"
    ];
    kernel.sysctl = {
      "kernel.yama.ptrace_scope" = 1; # 0 para permitir depuradores adherirse a procesos activos
      "vm.max_map_count" = 16777216;
      "fs.file-max" = 524288;
    };
  };

  zramSwap = {
    enable = true;
    memoryMax = 16 * 1024 * 1024 * 1024; # 16 GB ZRAM
  };

  # systemd.tmpfiles.rules =
  #   [ "L+ /run/gdm/.config/monitors.xml - - - - ${monitorsConfig}" ];
  systemd.tmpfiles.rules = [ "L+    /opt/rocm/hip   -    -    -     -    ${pkgs.rocmPackages.clr}" ];

  systemd.services.plantuml = {
    enable = true;
    description = "PlantUML server";
    requires = [ "docker.service" ];
    after = [ "docker.service" ];

    serviceConfig = {
      ExecStart = "${pkgs.docker}/bin/docker run --rm --name plantuml -p 8687:8080 plantuml/plantuml-server:jetty";
      ExecStop = "${pkgs.docker}/bin/docker stop -t 2 plantuml";
      Restart = "always";
    };

    wantedBy = [ "default.target" ];
  };

  systemd.services.ollama = {
    enable = false;
    description = "ollama server";
    requires = [ "docker.service" ];
    after = [ "docker.service" ];

    serviceConfig = {
      ExecStart = "${pkgs.docker}/bin/docker run --rm --gpus=all -v ollama:/root/.ollama -p 11434:11434 --name ollama ollama/ollama";
      ExecStop = "${pkgs.docker}/bin/docker stop -t 2 ollama";
      Restart = "always";
    };

    wantedBy = [ "default.target" ];
  };

  systemd.services.ollama-ui = {
    enable = false;
    description = "ollama web ui";
    requires = [
      "docker.service"
      "ollama.service"
    ];
    after = [
      "docker.service"
      "ollama.service"
    ];

    serviceConfig = {
      ExecStart = "${pkgs.docker}/bin/docker run --rm -p 3999:8080 --name ollama-webui ollama-webui";
      ExecStop = "${pkgs.docker}/bin/docker stop -t 2 ollama-webui";
      Restart = "always";
    };

    wantedBy = [ "default.target" ];
  };

  systemd.user.services.polkit-kde-authentication-agent-1 = {
    enable = false;
    description = "polkit-kde-authentication-agent-1";
    wantedBy = [ "graphical-session.target" ];
    wants = [ "graphical-session.target" ];
    after = [ "graphical-session.target" ];
    serviceConfig = {
      Type = "simple";
      ExecStart = "${pkgs.kdePackages.polkit-kde-agent-1}/libexec/polkit-kde-authentication-agent-1";
      Restart = "on-failure";
      RestartSec = 1;
      TimeoutStopSec = 10;
    };
  };

  networking.hostName = "woody"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/Costa_Rica";

  # Select internationalisation properties.
  i18n.defaultLocale = "es_CR.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "es_CR.UTF-8";
    LC_IDENTIFICATION = "es_CR.UTF-8";
    LC_MEASUREMENT = "es_CR.UTF-8";
    LC_MONETARY = "es_CR.UTF-8";
    LC_NAME = "es_CR.UTF-8";
    LC_NUMERIC = "es_CR.UTF-8";
    LC_PAPER = "es_CR.UTF-8";
    LC_TELEPHONE = "es_CR.UTF-8";
    LC_TIME = "es_CR.UTF-8";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.videoDrivers = [ "amdgpu" ];
  hardware.graphics = {
    enable = true;
    extraPackages = with pkgs; [ rocmPackages.clr.icd ];
  };

  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;
  # habilita hyprland
  programs.hyprland = {
    enable = true;
    package = pkgs.unstable.hyprland;
  };
  # programs.hyprlock.enable = false;
  # services.hypridle.enable = false;

  # # Configure keymap in X11
  services.xserver.xkb = {
    layout = "us";
    variant = "";
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.drivers = [ pkgs.hplip ];
  services.avahi.enable = true;
  services.avahi.nssmdns4 = true;
  # for a WiFi printer
  services.avahi.openFirewall = true;

  services.blueman.enable = true;

  # Bluetooth
  hardware.bluetooth.enable = true;
  hardware.bluetooth.powerOnBoot = true;

  # activa accesorios para Xbox one
  hardware.xone.enable = true;

  # activa la app de Corsair
  hardware.ckb-next.enable = true;

  # Steam
  hardware.steam-hardware.enable = true;
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    gamescopeSession.enable = true;
  };

  security.rtkit.enable = true;
  security.pam.mount.additionalSearchPaths = [ pkgs.bindfs ];
  security.doas.enable = false;
  security.doas.extraRules = [
    {
      users = [ config.users.users.jorge.name ];
      keepEnv = true;
    }
  ];
  security.sudo.enable = true;
  hardware.pulseaudio.enable = false;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };
  services.syncthing = {
    enable = false;
    user = config.users.users.jorge.name;
    openDefaultPorts = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Ledger
  hardware.ledger.enable = true;

  # habilita ZSH
  programs.zsh.enable = true;

  # habilita kdeconnect
  programs.kdeconnect = {
    enable = true;
    package = pkgs.gnomeExtensions.gsconnect; # for GNOME
  };

  programs.droidcam.enable = true;

  #programs.seahorse.enable = true; # for GNOME

  programs.gnupg.agent.enable = true;

  programs.gnupg.dirmngr.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.jorge = {
    isNormalUser = true;
    description = "Jorge Javier Araya Navarro";
    extraGroups = [
      "networkmanager"
      "wheel"
      "docker"
      "libvirtd"
      "realtime"
      "media"
      "cdrom"
    ];
    shell = pkgs.zsh;
    name = "jorge";
    hashedPasswordFile = config.sops.secrets."users/jorge/password_file".path;
    packages = [
      # tricks override to fix audio
      # see https://github.com/fufexan/nix-gaming/issues/165#issuecomment-2002038453
      (inputs.nix-gaming.packages.${pkgs.hostPlatform.system}.star-citizen.override {
        tricks = [
          "arial"
          "vcrun2019"
          "win10"
          "sound=alsa"
        ];
      })
    ];
  };

  environment.sessionVariables = {
    NIXOS_OZONE_WL = "1";
    QT_QPA_PLATFORM = "wayland;xcb";
    QT_QPA_PLATFORMTHEME = "qt5ct";
  };

  services.udev.packages = with pkgs; [ gnome-settings-daemon ];

  fonts.packages = with pkgs; [
    the-neue-black
    comic-neue
    google-fonts
    dotcolon-fonts
    libertinus
    junicode
    hachimarupop
    eunomia
  ];

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages =
    with pkgs;
    [
      git # requerido para nix flake
      git-crypt
      sops
      home-manager
      inputs.compose2nix.packages.x86_64-linux.default

      libsForQt5.qt5ct # Qt5 Configuration Tool
      adwaita-qt # Qt5 themes
      adwaita-qt6

      gpu-viewer
      ventoy-full
      fontfinder

      brasero
      dvdplusrwtools

      # para hyprland
      kitty
      wofi
      nwg-displays
      pavucontrol
      dex
      hyprwall
      hyprpaper
      swaynotificationcenter
      hyprpolkitagent

      ## Para C++
      unstable.llvmPackages_19.clang-tools
      unstable.llvmPackages_19.clang-manpages
      unstable.llvmPackages_19.clang-unwrapped
      gnumake
      cmake
      cmake-language-server
      codeblocks

      # comunicación en el trabajo
      zoom-us
      slack

      # comunicación con amigos y familiares
      tdesktop
      signal-desktop

      socat
      walker

      gnomeExtensions.appindicator
      gnomeExtensions.no-overview
      audacity
      bindfs
      blender-hip
      bottles
      brave
      bzip2
      ckb-next
      clipgrab
      coreutils
      cryptsetup
      curl
      dig
      discord
      docker-compose
      easyeffects
      evince
      ffmpeg-full
      firefox
      fwupd
      gparted
      hplip
      hunspell
      hunspellDicts.en_US
      hunspellDicts.es_CR
      hunspellDicts.es_ES
      iconpack-obsidian
      inkscape
      kbfs
      kdiskmark
      keybase-gui
      kubecolor
      kubectl
      kubectx
      libreoffice-fresh
      kdePackages.kdenlive
      mlt
      lm_sensors
      lua53Packages.digestif
      lutris
      heroic
      mangohud
      mediainfo
      mplayer
      mpv
      nixos-grub2-theme
      (wrapOBS {
        plugins = with obs-studio-plugins; [
          obs-source-record
          obs-pipewire-audio-capture
          obs-3d-effect
          obs-multi-rtmp
          obs-gradient-source
          input-overlay
          obs-vkcapture
          obs-gstreamer
          obs-vaapi
          obs-composite-blur
          obs-shaderfilter
        ];
      })
      obsidian
      theme-obsidian2
      openssl
      openjdk21
      pandoc
      pciutils
      pdfarranger
      prismlauncher
      protonmail-bridge-gui
      protontricks
      protonup-qt
      protonvpn-gui
      qt6.qttools
      radicle-node
      rclone
      readline
      remnote
      scrcpy
      smplayer
      sqlite
      syncthingtray
      texlab
      texlive.combined.scheme-full
      times-newer-roman
      virt-manager
      vlc
      wget
      wine
      wine64
      winetricks
      xournalpp
      xsane
      xz
      zlib
      gnome-tweaks
      gnome-boxes
      nil
      freecad
      gimp-with-plugins
      godot_4
      ledger-live-desktop
      helvum
      sonobus
      coppwr
      yt-dlp
      cheese
      v4l-utils
      mkvtoolnix
      makemkv
      mkclean
      handbrake
      alass
      grimblast
      wl-clipboard-rs
      wsrepl
      gramps # arboles genealogicos
      jellyfin-ffmpeg
      libva-utils
      reaper
    ]
    ++ (with unstable; [
      itch # broth.itch.ovh esta caido
    ]);

  # coloca una biblioteca en /var para mi uso personal
  fileSystems = {
    "/home/jorge/biblioteca" = {
      fsType = "fuse.bindfs";
      device = "/biblioteca/jorge-large";
      options = [
        "force-user=${config.users.users.jorge.name}"
        "force-group=users"
        "create-for-user=root"
        "create-for-group=root"
      ];
    };
    "/home/jorge/Medios" = {
      fsType = "fuse.bindfs";
      device = "/biblioteca/medios";
      options = [
        "force-user=${config.users.users.jorge.name}"
        "force-group=users"
        "create-for-user=media"
        "create-for-group=media"
      ];
    };
  };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    settings = {
      X11Forwarding = true;
      PermitRootLogin = "no";
      PasswordAuthentication = false;
    };
  };
  virtualisation.docker = {
    enable = true;
    enableOnBoot = true;
    autoPrune.enable = true;
  };
  services.flatpak.enable = true;
  services.shiori.enable = true;

  ## Todo en uno para coleccionar medios y visualizarlos ##
  users.users.media = {
    description = "media es un usuario que agrupa todas las aplicaciones de medios";
    isNormalUser = true;
    uid = 2001;
    createHome = true;
    home = "/biblioteca/medios";
    group = "media";
    extraGroups = [
      "render"
      "video"
    ];
    shell = "/bin/sh";
  };

  users.groups = {
    # grupo para todo lo asociado a medios
    media = {
      gid = 2001;
    };
  };

  # Visualizador de medios
  services.jellyfin = {
    enable = true;
    user = "media";
    group = "media";
    openFirewall = true;
    dataDir = "/biblioteca/medios/jellyfin";
  };

  systemd.services.flaresolverr = {
    enable = true;
    description = "resuelve retos de CloudFlare";
    requires = [ "docker.service" ];
    after = [ "docker.service" ];

    serviceConfig = {
      ExecStart = "${pkgs.docker}/bin/docker run --name=flaresolverr -p 8191:8191 -e LOG_LEVEL=info ghcr.io/flaresolverr/flaresolverr:latest";
      ExecStop = "${pkgs.docker}/bin/docker stop -t 2 flaresolverr";
      Restart = "always";
    };
  };

  # un "Grabador de video digital" inteligente para Usenet y BitTorrent
  services.sonarr = {
    enable = true;
    user = "media";
    group = "media";
    dataDir = "/biblioteca/medios/sonarr";
    openFirewall = true;
  };

  # manejador de colección de peliculas para Usenet y BitTorrent
  services.radarr = {
    enable = true;
    package = pkgs.unstable.radarr;
    user = "media";
    group = "media";
    dataDir = "/biblioteca/medios/radarr";
    openFirewall = true;
  };

  # para música
  services.lidarr = {
    enable = true;
    package = pkgs.unstable.lidarr;
    user = "media";
    group = "media";
    dataDir = "/biblioteca/medios/lidarr";
    openFirewall = true;
  };

  # para manejar los subtítulos
  services.bazarr = {
    enable = true;
    user = "media";
    group = "media";
    openFirewall = true;
  };

  # central de busqueda por indices, solicitudes que van a sonarr y radarr
  services.prowlarr = {
    enable = true;
    package = pkgs.unstable.prowlarr;
    openFirewall = true;
  };

  # para bajar los torrents
  services.transmission = {
    enable = true;
    user = "media";
    group = "media";
    home = "/biblioteca/medios/transmission";
    openFirewall = true;
    openRPCPort = true;
    openPeerPorts = true;
    settings = {
      rpc-bind-address = "0.0.0.0";
      rpc-whitelist = "127.0.0.1,192.168.*.*";
    };
  };
  ## fin

  virtualisation.libvirtd.enable = true;
  programs.dconf.enable = true;
  programs.java.enable = true;

  programs.gamemode = {
    enable = true;
    settings = {
      custom = {
        start = "${pkgs.libnotify}/bin/notify-send 'GameMode iniciado'";
        end = "${pkgs.libnotify}/bin/notify-send 'GameMode terminado'";
      };
    };
  };

  services.postgresql = {
    enable = true;
    enableTCPIP = true;
    authentication = pkgs.lib.mkOverride 10 ''
      #type database  DBuser  auth-method
      local all       all     trust
      # ipv4
      host  all      all     127.0.0.1/32   trust
      # ipv6
      host all       all     ::1/128        trust
    '';
    initialScript = pkgs.writeText "backend-initScript" ''
      CREATE ROLE "p2p-scout" WITH LOGIN NOCREATEROLE NOCREATEDB NOSUPERUSER NOINHERIT NOREPLICATION;
      CREATE DATABASE "p2p-scout";
      GRANT ALL PRIVILEGES ON DATABASE "p2p-scout" TO "p2p-scout";
    '';
  };

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [
    22
    8384
    22000
    24800
    8080
    3000
    5173 # svelte/kit dev
  ];
  networking.firewall.allowedUDPPorts = [
    22000
    21027
  ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}
