{ pkgs, ... }:

{
  programs.bat = {
    enable = true;
    config = {
      tabs = "4";
    };
    extraPackages = [
      pkgs.bat-extras.batdiff
      pkgs.bat-extras.batman
      pkgs.bat-extras.batgrep
      pkgs.bat-extras.batwatch
      pkgs.bat-extras.prettybat
    ];
  };
}
