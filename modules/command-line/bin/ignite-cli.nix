{ bash }:
''
  #!${bash}/bin/bash

  set -e

  cli_home=$HOME/.cache/ignite
  cli_version=v0.27.1
  run_serve=false

  for arg in "$@"; do
    if [ "$arg" = "serve" ]; then
      run_serve=true
      break
    fi
  done

  if [ ! -d "$cli_home" ]; then
      mkdir -p "$cli_home"
  fi

  # cross the directory tree until it finds the project
  project_home="."
  while [[ "$project_home" != "/" ]]; do
    for file in "config.yml" "go.mod"; do
      if [[ -e "$project_home/$file" ]]; then
        break 2
      fi
    done
    last_project_home="$project_home"
    project_home=$(realpath "$project_home/..")
    if [[ "$project_home" == "$last_project_home" ]]; then
      break
    fi
  done

  project_home=$(realpath "$project_home")

  # use $PWD in case no project was found
  project_found=$PWD
  if [ -e "$project_home/config.yml" ]; then
     project_found=$project_home
  fi

  if [ "$run_serve" = true ]; then
      faucet_port=$(yq '.faucet.port // 4500' "$project_home/config.yml")
      cosmos_port=1317
      tendermint_port=26657

      docker run --rm -ti \
        -v "$cli_home":/home/tendermint \
        -v "$project_found":/apps \
        -p $cosmos_port:$cosmos_port \
        -p $tendermint_port:$tendermint_port \
        -p $faucet_port:$faucet_port \
        ignitehq/cli:$cli_version "$@"
  else
      docker run --rm -ti \
        -v "$cli_home":/home/tendermint \
        -v "$project_found":/apps \
        ignitehq/cli:$cli_version "$@"
  fi
''
