{ pkgs, ... }:

{
  home.packages = with pkgs; [
    xcp
    ripgrep
    tree
    fd
    jq
    yq-go
    d2
    qpdf
    tldr
  ];

  home.sessionPath = [ "$HOME/.local/hm/bin" ];
  programs.zsh.shellAliases = {
    z = "zellij";
    cp = "xcp";
  };

  programs.zellij = {
    enable = true;
    enableZshIntegration = false;
    package = pkgs.unstable.zellij;

    settings = {
      default_mode = "locked";
      simplified_ui = true;
      ui.pane_frames.rounded_corners = true;
    };
  };

  home.file = {
    "ignite-cli" = {
      enable = true;
      executable = true;
      target = ".local/hm/bin/ignite";
      text = import ./bin/ignite-cli.nix { bash = pkgs.bash; };
    };
    "worktree-clone" = {
      enable = true;
      executable = true;
      target = ".local/hm/bin/worktree-clone";
      text = import ./bin/git-worktree-clone.nix { bash = pkgs.bash; };
    };
  };
}
