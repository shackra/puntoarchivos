{ pkgs, ... }:

{
  home.packages = [
    pkgs.unstable.nixdoc
    pkgs.unstable.nixfmt-rfc-style
    pkgs.unstable.alejandra
    pkgs.unstable.nil
  ];
}
