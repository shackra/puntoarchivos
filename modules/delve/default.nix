{ ... }:

{
  home.file = {
    delve = {
      enable = true;
      executable = false;
      target = ".config/dlv/config.yml";
      source = ./file.yml;
    };
  };
}
