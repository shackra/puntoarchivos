{ pkgs, ... }:
{
  home.packages = [
    pkgs.go
    pkgs.gopls
    pkgs.gofumpt
    pkgs.golines
    pkgs.golangci-lint
    pkgs.golangci-lint-langserver
    pkgs.delve
  ];
}
