{ pkgs, ... }:

{
  imports = [
    ./alacritty
    ./bat
    ./command-line
    ./command-line-dev
    ./delve
    ./direnv
    ./emacs
    ./eza
    ./fonts
    ./git
    ./golang
    ./gui-dev
    ./ollama
    ./starship
    ./zsh
    ./python
    ./nix
    ./rofi
  ];

  home.packages = [ pkgs.stdenv.cc ];
}
