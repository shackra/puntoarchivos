{
  pkgs,
  ...
}:

{
  programs.alacritty = {
    enable = true;
    package = pkgs.alacritty;
    settings = {
      general = {
        live_config_reload = true;
        import = [ "~/.config/alacritty/catppuccin-mocha.toml" ];
      };
      env = {
        TERM = "xterm-256color";
      };
      bell = {
        animation = "EaseOutQuart";
        color = "0xffffff";
        duration = 1;
      };

      cursor = {
        unfocused_hollow = true;

        style = {
          blinking = "On";
          shape = "Block";
        };
      };

      debug = {
        log_level = "Off";
        persistent_logging = false;
        render_timer = false;
      };

      font = {
        size = 14;

        bold = {
          family = "Iosevka Nerd Font Mono";
          style = "Bold";
        };

        italic = {
          family = "Iosevka Nerd Font Mono";
          style = "Italic";
        };

        normal = {
          family = "Iosevka Nerd Font Mono";
          style = "Regular";
        };
      };

      mouse = {
        hide_when_typing = true;
        bindings = [
          {
            action = "PasteSelection";
            mouse = "Middle";
          }
        ];
      };

      scrolling = {
        multiplier = 5;
      };

      selection = {
        save_to_clipboard = true;
        semantic_escape_chars = '',│`|:" ()[]{}<>'';
      };

      terminal = {
        shell = {
          args = [ "--login" ];
          program = "${pkgs.zsh}/bin/zsh";
        };
      };

      window = {
        decorations = "full";
        dynamic_padding = false;
        opacity = 0.95;
        blur = true;
      };
      hints = {
        enabled = [
          {
            command = "xdg-open"; # On Linux/BSD
            hyperlinks = true;
            post_processing = true;
            persist = false;
            mouse.enabled = true;
            regex = "(ipfs:|ipns:|magnet:|mailto:|gemini://|gopher://|https://|http://|news:|file:|git://|ssh:|ftp://)[^\\u0000-\\u001F\\u007F-\\u009F<>`]+";
          }
        ];
      };

      keyboard = {
        bindings = [
          {
            key = "Return";
            mods = "Control|Shift";
            action = "SpawnNewInstance";
          }
        ];
      };
    };
  };
  home.file = {
    "catppuccin theme" = {
      enable = true;
      target = ".config/alacritty/catppuccin-mocha.toml";
      source = ./conf/catppuccin-mocha.toml;
    };
  };
}
