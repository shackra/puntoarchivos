{
  config,
  lib,
  pkgs,
  ...
}:

{
  programs.zsh = {
    enable = true;

    enableCompletion = false; # ver https://github.com/marlonrichert/zsh-autocomplete?tab=readme-ov-file#note-for-nixos
    autosuggestion.enable = true;
    syntaxHighlighting.enable = true;
    enableVteIntegration = true;
    defaultKeymap = "emacs";

    dirHashes = {
      emacs = "$HOME/.config/doom";
      doom = "$HOME/.config/emacs";
      dotfiles = "$HOME/puntoarchivos";
    };

    history = {
      size = 1000000;
      save = 1000000;
      path = "${config.xdg.dataHome}/zsh/history";
      ignoreDups = true;
      ignorePatterns = [
        "rm *"
        "pkill *"
        "killall *"
        "cd *"
        "mv *"
      ];
      ignoreSpace = true;
    };
    shellAliases = {
      ip = "ip --color=auto";
      mv = "mv -i";
      rm = " rm -v";
      ln = "ln -i";
      chown = "chown --preserve-root";
      chmod = "chmod --preserve-root";
      chgrp = "chgrp --preserve-root";
      cls = ''
        echo -ne "\033c"
      '';
    };

    zplug = {
      enable = true;
      plugins = [
        { name = "marlonrichert/zsh-autocomplete"; }
        { name = "ntnyq/omz-plugin-pnpm"; }
        { name = "ptavares/zsh-direnv"; }
        {
          name = "plugins/brew";
          tags = [
            "from:oh-my-zsh"
            ''if:"[[ $OSTYPE == *darwin* ]]"''
          ];
        }
        {
          name = "plugins/deno";
          tags = [ "from:oh-my-zsh" ];
        }
        {
          name = "plugins/python";
          tags = [ "from:oh-my-zsh" ];
        }
        {
          name = "plugins/rust";
          tags = [ "from:oh-my-zsh" ];
        }
        {
          name = "plugins/node";
          tags = [ "from:oh-my-zsh" ];
        }
        {
          name = "plugins/npm";
          tags = [ "from:oh-my-zsh" ];
        }
        {
          name = "plugins/yarn";
          tags = [ "from:oh-my-zsh" ];
        }
      ];
    };

    initExtra = ''
      eval "$(${pkgs.zoxide}/bin/zoxide init zsh --cmd d)"
      bindkey "''${key[Up]}" fzf-history-widget
    '';
  };

  programs.fzf = {
    enable = true;
    enableZshIntegration = true;
    defaultOptions = [
      "--height 25%"
      "--layout=reverse"
      "--border"
      "--inline-info"
    ];
  };

  home.packages = with pkgs; [ zoxide ];
}
