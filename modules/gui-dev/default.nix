{
  inputs,
  outputs,
  config,
  lib,
  pkgs,
  ...
}:

{
  nixpkgs.config.allowUnfreePredicate =
    pkg: builtins.elem (pkgs.lib.getName pkg) [ "mongodb-compass" ];

  home.packages = [
    pkgs.mongodb-compass
    pkgs.unstable.dbeaver-bin
  ];
}
