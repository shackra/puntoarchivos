{
  inputs,
  outputs,
  config,
  pkgs,
  ...
}:

{
  home.packages = with pkgs; [
    (unstable.python312.withPackages (
      ps: with ps; [
        python
        virtualenv
        grip
      ]
    ))
    unstable.poetry
    unstable.poethepoet
  ];
}
