{
  config,
  lib,
  pkgs,
  ...
}:

{
  home.file = {
    "ollama-cli" = {
      enable = true;
      executable = true;
      target = ".local/hm/bin/ollama";
      text = ''
        #!${pkgs.bash}/bin/bash

        docker exec -it ollama ollama "$@"
      '';
    };
  };
  home.sessionPath = [ "$HOME/.local/hm/bin" ];
}
