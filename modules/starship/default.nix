{
  config,
  lib,
  pkgs,
  ...
}:

{
  programs.starship = {
    enable = true;

    # Configuration written to ~/.config/starship.toml
    settings = {
      add_newline = true;

      character = {
        success_symbol = "[➜](flamingo) ";
        error_symbol = "[💀](red) ";
      };

      git_commit = {
        tag_disabled = false;
        only_detached = false;
      };
    };
  };
}
