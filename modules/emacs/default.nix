{
  config,
  lib,
  pkgs,
  ...
}:
let
  defaultPkg = (
    (pkgs.emacsPackagesFor pkgs.emacs29-gtk3).emacsWithPackages (
      epkgs: with epkgs; [
        (treesit-grammars.with-grammars (grammars: with grammars; [ tree-sitter-gdscript ]))
        sqlite3
        vterm
        pkgs.unstable.emacsPackages.mu4e
      ]
    )
  );
in
{
  services.emacs = {
    enable = true;
    package = defaultPkg;
  };

  programs.emacs = {
    enable = true;
    package = defaultPkg;
  };

  fonts.fontconfig.enable = true;

  home.packages = [
    # fuentes
    (pkgs.nerdfonts.override {
      fonts = [
        "FiraCode"
        "DroidSansMono"
        "Iosevka"
      ];
    })
    pkgs.iosevka-comfy.comfy
    pkgs.iosevka-comfy.comfy-motion-duo
    pkgs.iosevka
    pkgs.julia-mono

    # dependencias de Emacs
    (pkgs.aspellWithDicts (
      dicts: with dicts; [
        en
        en-computers
        en-science
        es
      ]
    ))
    pkgs.shellcheck
    pkgs.binutils
    pkgs.cmake
    pkgs.fd
    pkgs.imagemagick
    (lib.mkIf (config.services.gpg-agent.enable) pkgs.pinentry-emacs)
    pkgs.zstd

    # dependencias de Doom Emacs
    pkgs.plantuml
    pkgs.html-tidy
    pkgs.nodePackages_latest.stylelint
    pkgs.nodePackages_latest.js-beautify
    pkgs.nodePackages_latest.yarn
    pkgs.nodePackages_latest.yalc
    pkgs.nodePackages_latest.prettier
    (pkgs.ripgrep.override { withPCRE2 = true; })
    pkgs.semgrep
    pkgs.editorconfig-core-c
    pkgs.texlive.combined.scheme-medium

    # servidores lsp
    pkgs.nodePackages_latest.bash-language-server
    pkgs.nodePackages_latest.typescript-language-server
    pkgs.yaml-language-server
    pkgs.nodePackages_latest.vscode-json-languageserver
    pkgs.vscode-langservers-extracted
  ];

  # copiado de este articulo:
  # https://bhankas.org/blog/deploying_doom_emacs_config_via_nixos_home_manager/
  home = {
    sessionVariables = {
      EDITOR = "${defaultPkg}/bin/emacs";
    };
  };
}
