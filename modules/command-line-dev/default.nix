{
  config,
  lib,
  pkgs,
  ...
}:

{
  home.packages = with pkgs; [
    lazydocker
    gh
    glab
  ];
}
