{
  config,
  lib,
  pkgs,
  ...
}:

let
  defaultEmacsPkg = pkgs.unstable.emacs28-gtk3;
  defaultCoreutilsPkg = pkgs.coreutils;
in
{
  home.packages = [ defaultCoreutilsPkg ];
  home.file = {
    "ediff" = {
      enable = true;
      executable = true;
      target = ".local/hm/bin/ediff.sh"; # de https://github.com/paulotome/emacstool
      text = import ./ediff.nix {
        emacs = defaultEmacsPkg;
        coreutils = defaultCoreutilsPkg;
      };
    };
  };
  programs.git = {
    enable = true;
    package = pkgs.gitAndTools.gitFull;
    userEmail = "jorge@esavara.cr";
    userName = "Jorge Javier Araya Navarro";

    extraConfig = {
      core = {
        whitespace = "trailing-space,space-before-tab";
      };
      color = {
        ui = "auto";
      };
      rerere = {
        enabled = "true";
      };
      rebase = {
        autoSquash = "true";
      };
      push = {
        autoSetupRemote = true;
      };
      merge = {
        conflictStyle = "diff3";
        tool = "ediff";
      };
      "mergetool \"ediff\"" = {
        cmd = "~/.local/hm/bin/ediff.sh $LOCAL $REMOTE $MERGED";
        trustExitCode = true;
      };
      diff = {
        tool = "ediff";
        guitool = "ediff";
      };
      "difftool \"ediff\"" = {
        cmd = "~/.local/hm/bin/ediff.sh $LOCAL $REMOTE $MERGED";
      };
      pull = {
        rebase = true;
      };
      github = {
        user = "shackra";
      };
      init.defaultBranch = "master"; # a classic
    };

    lfs = {
      enable = true;
    };
    aliases = {
      worktree-clone = "!sh $HOME/.local/hm/bin/worktree-clone";
    };
  };

}
