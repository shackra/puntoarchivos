{
  description = "La configuración de Jorge para Home Manager y NixOS";

  inputs = {
    # Specify the source of Home Manager and Nixpkgs.
    ucodenix.url = "github:e-tho/ucodenix";
    nix-gaming.url = "github:fufexan/nix-gaming";
    nixpkgs.url = "https://flakehub.com/f/NixOS/nixpkgs/0.2411.*.tar.gz";
    nixpkgs-unstable.url = "https://flakehub.com/f/NixOS/nixpkgs/0.1.*.tar.gz";
    home-manager = {
      url = "https://flakehub.com/f/nix-community/home-manager/0.2411.*.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # fh de flakehub
    fh.url = "https://flakehub.com/f/DeterminateSystems/fh/*.tar.gz";
    # language server protocol para Nix
    nixd.url = "github:nix-community/nixd";
    # sops
    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # Nix User Respository
    nur.url = "github:nix-community/NUR";
    # catppuccin
    catppuccin.url = "github:catppuccin/nix";
    nix-search.url = "github:diamondburned/nix-search";
    # lix is a better nix
    lix-module = {
      url = "https://git.lix.systems/lix-project/nixos-module/archive/2.91.1-1.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    compose2nix.url = "github:aksiksi/compose2nix";
    compose2nix.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs =
    {
      self,
      nixpkgs,
      nixpkgs-unstable,
      catppuccin,
      home-manager,
      sops-nix,
      nur,
      nixd,
      lix-module,
      nix-gaming,
      ucodenix,
      ...
    }@inputs:
    let
      inherit (self) outputs;
    in
    {
      # NixOS configuration.
      nix.nixPath = [ "nixpkgs=${inputs.nixpkgs}" ];
      inherit nixpkgs;
      inherit nixpkgs-unstable;

      overlays = import ./overlays.nix { inherit inputs; };

      # system configurations
      nixosConfigurations.woody = nixpkgs.lib.nixosSystem {
        specialArgs = {
          inherit inputs outputs;
        };
        modules = [
          { nixpkgs.overlays = [ nixd.overlays.default ]; }
          nur.modules.nixos.default
          sops-nix.nixosModules.sops
          lix-module.nixosModules.default
          ./system/hosts/woody/configuration.nix
        ];
      };

      # home-manager
      homeConfigurations = {
        "jorge" = home-manager.lib.homeManagerConfiguration {
          pkgs = nixpkgs.legacyPackages.x86_64-linux;
          extraSpecialArgs = {
            inherit inputs outputs;
          };
          modules = [
            catppuccin.homeManagerModules.catppuccin
            ./homes/woody/home.nix
          ];
        };
      };
    };
}
